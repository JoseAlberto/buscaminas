#include "EndState.h"
#include "PlayState.h"
#include "MenuState.h"
#include "GameManager.h"

template<> EndState* Ogre::Singleton<EndState>::msSingleton = 0;

void EndState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    
    Tablero::getSingletonPtr()->limpiarTablero();
    
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");

    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

	if(!_creado){
        createGUI();
        _creado=true;
    }//Fin if
    
    attachButtons();
    ocultarBotones();
    ocultarRecords();
    if(Tablero::getSingletonPtr()->getCClick() == 0){
        std::cout << "Has ganado!! :D" << std::endl;
           mostrarRecords();   
    } else {
        std::cout << "Has perdido!! Dx" << std::endl;
        mostrarBotones();
    }//Fin if
    
	CEGUI::MouseCursor::getSingleton().show();
    _exitGame= false;
}//Fin enter

void EndState::createGUI(){

	_sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "SheetEndState"); 
    
	_editBox = CEGUI::WindowManager::getSingleton().loadWindowLayout("MystringFormat.layout");    
	CEGUI::WindowManager::getSingleton().getWindow(
        "FormatWinBox/Text")->setText("[colour='FF0000FF'][font='Batang-26] Desea jugar de nuevo?");

	_quitButton = CEGUI::WindowManager::getSingleton().getWindow("FormatWinBox/ExitButton");
  
    _quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&EndState::salir, this));


	_acceptButton = CEGUI::WindowManager::getSingleton().getWindow("FormatWinBox/AcceptButton");
   
    _acceptButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&EndState::mostrarMenu,this)); 

    _recordText = CEGUI::WindowManager::getSingleton().loadWindowLayout("RecordFormat.layout");

    _acceptRecordButton = CEGUI::WindowManager::getSingleton().getWindow("FormatRecordBox/AcceptButton");
  
    _acceptRecordButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&EndState::mostrarVolver,this));      

    CEGUI::WindowManager::getSingleton().getWindow(
        "FormatRecordBox/Text")->setText("[colour='FF0000FF'][font='Batang-26] Introduzca su nombre");
   
    _nameText = static_cast<CEGUI::Editbox*>(CEGUI::WindowManager::getSingleton().getWindow("FormatRecordBox/EditBox"));
}//Fin createGUI

void EndState::attachButtons(){
	_sheet->addChildWindow(_editBox);
    _sheet->addChildWindow(_recordText);    

    CEGUI::System::getSingleton().setGUISheet(_sheet);
}//Fin attachButtons

void EndState::exit (){
    _sceneMgr->clearScene();
}//Fin exit

void EndState::pause (){}
 
void EndState::resume (){}

bool EndState::frameStarted(const Ogre::FrameEvent& evt){
	CEGUI::System::getSingleton().injectTimePulse(_timeSinceLastFrame);
     
    return true;
}//Fin frameStarted

bool EndState::frameEnded(const Ogre::FrameEvent& evt){  
	if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void EndState::keyPressed(const OIS::KeyEvent &e){
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text); 
}//Fin keyPressed

void EndState::keyReleased(const OIS::KeyEvent &e){
    CEGUI::System::getSingleton().injectKeyUp(e.key);
}//Fin keyReleased

void EndState::mouseMoved(const OIS::MouseEvent &e){
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}//Fin mouseMoved

void EndState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}//Fin mouseMoved

void EndState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}//Fin mousePressed

EndState* EndState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

EndState& EndState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

CEGUI::MouseButton EndState::convertMouseButton(OIS::MouseButtonID id){
    CEGUI::MouseButton ceguiId;

    switch(id){
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }//Fin switch
  return ceguiId;
}//Fin convertMouseButton


bool EndState::mostrarMenu(const CEGUI::EventArgs &e){
	ocultarBotones();

	CEGUI::MouseCursor::getSingleton().hide();
	popState();
	return true;
}//Fin mostrarMenu
        

bool EndState::salir(const CEGUI::EventArgs &e){
	_exitGame = true;
	return true;	
}//Fin salir

void EndState::ocultarBotones(){
	_editBox->hide();	
}//Fin ocultarBotones

void EndState::mostrarBotones(){
	_editBox->show();	
}//Fin mostrarBotones

void EndState::mostrarRecords(){
    _recordText->show();
}//Fin mostrarRecords

void EndState::ocultarRecords(){
    _recordText->hide();
}//Fin ocultarRecords

bool EndState::mostrarVolver(const CEGUI::EventArgs &e){
    CEGUI::String nombre_capturado = _nameText->CEGUI::Editbox::getText();
    
    _nombre =_nameText->CEGUI::Editbox::getText().c_str();
    std::cout<<_nombre<<" "<<_puntuacion<<std::endl;
    GameManager::getSingletonPtr()->addRecord(_nombre, _puntuacion);
    ocultarRecords();
    _editBox->show();
    return true;
}//Fin mostrarVolver

void EndState::setPuntuacion(int puntu){
    _puntuacion=puntu;
}//Fin setPuntuacion
