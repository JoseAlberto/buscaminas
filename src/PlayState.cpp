#include "PlayState.h"
#include "EndState.h"
#include "PauseState.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter(){
    int tab_size = 15;
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _win = _root->getAutoCreatedWindow();

    /* Creamos el rayQuery */
    _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

    /* Creamos un tablero nuevo de 15x15 */
    Tablero::getSingletonPtr()->crearTablero(tab_size, _nbombs);

    /* Rellenamos la parte grafica */
    createScene(tab_size);
    createOverlay();

    t_ini = t_pausa = clock();

    _exitGame = false; _endGame = false;
}//Fin enter

void PlayState::createOverlay() {
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *overlay = _overlayManager->getByName("PTInfo");
    overlay->show();
}//Fin createOverlay

void PlayState::setBombs(int nbombs){
    _nbombs = nbombs;
}//Fin setBombs

void PlayState::createScene(int n_celdas) {
    double p_ini, x, z, tam_celda;
    std::stringstream sncelda, sentity;

    /* Variara dependiendo del n_celdas */
    _camera->setPosition(Ogre::Vector3(-2, 45, 70));
    _camera->lookAt(Ogre::Vector3(-2, -10, 0));

    /* Nodo base */
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo_cel = NULL;
    Ogre::SceneNode *nodo_tab = _sceneMgr->createSceneNode("Tablero");

    /* Tamaño base de celda y posicion esquina superior izquierda */
    tam_celda = 4.5; p_ini = (n_celdas * tam_celda)/2 * -1;

    /* Igualamos x y z a la posicion inicial */
    x = z = p_ini;
    for(int f = 0; f < n_celdas; f++){
        for(int c = 0; c < n_celdas; c++){
            /* Nombre del nodo para esa celda */
            sncelda << "C" << f << "_"<< c;
            /* Creamos el nodo para una determinada celda */
            nodo_cel = nodo_tab->createChildSceneNode(sncelda.str(), Ogre::Vector3(x, 0, z));
            /* Para ese nodo creamos sus dos posibles visualizaciones */
            /* Visualizacion estandar */
            ent = _sceneMgr->createEntity("Celda.mesh");
            ent->setQueryFlags(CLICK);
            nodo_cel->attachObject(ent);

            /* Visualizacion de estado, dependera del estado des tab[f][c] y no sera visible */
            switch(Tablero::getSingletonPtr()->getCelda(f, c)->getEstado()){
                case 0:
                    sentity << "0_Celda.mesh";
                    break;
                case 1:
                    sentity << "1_celda.mesh";
                    break;
                case 2:
                    sentity << "2_celda.mesh";
                    break;
                case 3:
                    sentity << "3_celda.mesh";
                    break;
                case 4:
                    sentity << "4_celda.mesh";
                    break;
                case 5:
                    sentity << "5_celda.mesh";
                    break;
                case 6:
                    sentity << "6_celda.mesh";
                    break;
                case 7:
                    sentity << "7_celda.mesh";
                    break;
                case 8:
                    sentity << "8_celda.mesh";
                    break;
                case -1:
                    sentity << "mine.mesh";
                    break;
            }//Fin switch

            ent = _sceneMgr->createEntity(sentity.str());
            ent->setQueryFlags(NCLICK);
            ent->setVisible(false);
            nodo_cel->attachObject(ent);

            /* Establecemos las relaciones */
            Tablero::getSingletonPtr()->getCelda(f, c)->setNodo(nodo_cel); 
            Tablero::getSingletonPtr()->addToMap(sncelda.str(), 
                Tablero::getSingletonPtr()->getCelda(f, c));
            /* Incrementamos indices y reiniciamos nombre nodo */
            x += tam_celda; sncelda.str(""); sentity.str("");
        }//Fin for
        x = p_ini; z += tam_celda;
    }//Fin for

    _sceneMgr->getRootSceneNode()->addChild(nodo_tab);

    Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, -0.001);
    Ogre::MeshManager::getSingleton().createPlane("plane1",
    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
    400,400,1,1,true,1,20,20,Ogre::Vector3::UNIT_Z);

    Ogre::SceneNode* nod_suelo = _sceneMgr->createSceneNode("ground");
    Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "plane1");
    groundEnt->setMaterialName("Ground");
    groundEnt->setQueryFlags(NCLICK);
    nod_suelo->attachObject(groundEnt);

    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE); 
    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(1,-1,0));
    nod_suelo->attachObject(light);

    _sceneMgr->getRootSceneNode()->addChild(nod_suelo);
}//Fin createScene

void PlayState::exit(){
    _sceneMgr->clearScene();
    _sceneMgr->destroyQuery(_raySceneQuery);

    Ogre::Overlay *overlay = _overlayManager->getByName("PTInfo");
    overlay->hide();
}//Fin exist

void PlayState::pause(){
    Ogre::Overlay *overlay = _overlayManager->getByName("PTInfo");
    overlay->hide();

    /* Cogemos el instante en el que se pausa */
    t_pausa = clock();
}//Fin pause

void PlayState::resume(){
    Ogre::Overlay *overlay = _overlayManager->getByName("PTInfo");
    overlay->show();

    /* Actualizamos el tiempo inicial en funcion del tiempo pausado */
    t_ini += clock() - t_pausa;
}//Fin resume

bool PlayState::frameStarted(const Ogre::FrameEvent& evt){
    if(!_endGame){
        int seg = ((int)(clock() - t_ini)) / CLOCKS_PER_SEC;
        _puntuacion=seg;
        Ogre::OverlayElement *oe;
        oe = _overlayManager->getOverlayElement("timeText");
        oe->setCaption(Ogre::StringConverter::toString(seg));
    }//Fin if

    return true;
}//Fin frameStarted

bool PlayState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void PlayState::keyPressed(const OIS::KeyEvent &e){
    Ogre::Vector3 vt(0,0,0);

    switch(e.key){
        case OIS::KC_P:
            /* Cambiamos al estado de pausa */
            if(!_endGame){
                pushState(PauseState::getSingletonPtr());
            }//Fin if
            break;
        case OIS::KC_UP:
            if(!_endGame){
                vt+=Ogre::Vector3(0, 1, 0);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_DOWN:
            if(!_endGame){
                vt+=Ogre::Vector3(0, -1, 0);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_LEFT:
            if(!_endGame){
                vt+=Ogre::Vector3(-1, 0, 0);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_RIGHT:
            if(!_endGame){
                vt+=Ogre::Vector3(1, 0, 0);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_MINUS:
            if(!_endGame){
                vt+=Ogre::Vector3(0, 0, 1);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_ADD:
            if(!_endGame){
                vt+=Ogre::Vector3(0, 0, -1);
                _camera->moveRelative(vt);
            }//Fin if
            break;
        case OIS::KC_SPACE:
            if(_endGame){
                Ogre::Overlay *overlay;
                
                if(Tablero::getSingletonPtr()->getCClick() == 0){
                    overlay = _overlayManager->getByName("Win");
                } else {
                    overlay = _overlayManager->getByName("Lose");
                }//Fin if
                overlay->hide();

                EndState::getSingletonPtr()->setPuntuacion(_puntuacion);
                changeState(EndState::getSingletonPtr());
            }//Fin if
        default:
            break;
    }//switch
}//Fin keyPressed

void PlayState::keyReleased(const OIS::KeyEvent &e){
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }//Fin if
}//Fin keyReleased

void PlayState::mouseMoved(const OIS::MouseEvent &e){
    Ogre::OverlayElement *oe;
    oe = _overlayManager->getOverlayElement("cursor");
    oe->setLeft(e.state.X.abs);  oe->setTop(e.state.Y.abs);
}//Fin mouseMoved

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    int posx = e.state.X.abs;
    int posy = e.state.Y.abs;

    /* Mascara de objetos que son BASICOS */
    Ogre::uint32 mask = CLICK;

    if(e.state.buttonDown(OIS::MB_Left) && !_endGame){
        Ogre::Ray rayMouse = _camera->getCameraToViewportRay(
            posx/float(_win->getWidth()), posy/float(_win->getHeight()));
        _raySceneQuery->setRay(rayMouse);
        _raySceneQuery->setSortByDistance(true);
        _raySceneQuery->setQueryMask(mask);

        Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();
        Ogre::RaySceneQueryResult::iterator it;
        it = result.begin();
        
        if(it != result.end()){
            _endGame = Tablero::getSingletonPtr()->Click(
                it->movable->getParentSceneNode()->getName());
        }//Fin if

        if(_endGame){
            Ogre::Overlay *overlay = _overlayManager->getByName("PTInfo");
            overlay->hide();

            /* Mostramos donde estaban las bombas */
            Tablero::getSingletonPtr()->swapMines();

            if(Tablero::getSingletonPtr()->getCClick() == 0){
                overlay = _overlayManager->getByName("Win");
            } else {
                overlay = _overlayManager->getByName("Lose");
            }//Fin if
            overlay->show();
        }//Fin if
    }//Fin if
}//Fin mousePressed

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){}

PlayState* PlayState::getSingletonPtr(){
    return msSingleton;
}//Fin getSingletonPtr

PlayState& PlayState::getSingleton(){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton