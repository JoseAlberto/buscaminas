#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "MenuState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "EndState.h"
#include "Tablero.h"

#include <iostream>

using namespace std;

int main(){

    Tablero* tablero = new Tablero();
    GameManager* game = new GameManager();
    IntroState* introState = new IntroState();
    MenuState* menuState = new MenuState();
    PlayState* playState = new PlayState();
    PauseState* pauseState = new PauseState();
    EndState* endState = new EndState();

    UNUSED_VARIABLE(tablero);
    UNUSED_VARIABLE(introState);
    UNUSED_VARIABLE(menuState);
    UNUSED_VARIABLE(playState);
    UNUSED_VARIABLE(pauseState);
    UNUSED_VARIABLE(endState);
        
    try{
        game->start(IntroState::getSingletonPtr());
    }catch (Ogre::Exception& e){
        std::cerr << "Excepción detectada: " << e.getFullDescription();
    }//Fin try-cath
      
    delete game;
      
    return 0;
}//Fin main
