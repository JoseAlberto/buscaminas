#include "PauseState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    createOverlay();

    _exitGame = false;
}//Fin enter

void PauseState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *overlay = _overlayManager->getByName("Pausa");
    overlay->show();
}//Fin createOverlay

void PauseState::exit (){
    Ogre::Overlay *overlay = _overlayManager->getByName("Pausa");
    overlay->hide();
}//Fin exit

void PauseState::pause (){}

void PauseState::resume (){}

bool PauseState::frameStarted(const Ogre::FrameEvent& evt){
    return true;
}//Fin frameStarted

bool PauseState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void PauseState::keyPressed(const OIS::KeyEvent &e){
    if (e.key == OIS::KC_P) {
        popState();
    }//Fin if 
}//Fin keyPressed

void PauseState::keyReleased(const OIS::KeyEvent &e){}

void PauseState::mouseMoved(const OIS::MouseEvent &e){}

void PauseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){}

void PauseState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){}

PauseState* PauseState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

PauseState& PauseState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton
