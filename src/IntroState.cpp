#include "IntroState.h"
#include "MenuState.h"
#include <iostream>

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

    _camera = _sceneMgr->createCamera("IntroCamera");
    _camera->setPosition(Ogre::Vector3(0, 0, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);

    createScene();

    _exitGame = false;
}//Fin enter

void IntroState::createScene(){
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *nodo = NULL;

    _camera->setPosition(Ogre::Vector3(0, 6.5, 20));
    _camera->lookAt(Ogre::Vector3(0, 6.5, 0));

    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "Titulo", Ogre::Vector3(0, 7, 0));
    ent = _sceneMgr->createEntity("Titulo.mesh");
    nodo->attachObject(ent);

    nodo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "PSpace", Ogre::Vector3(0, 5, 0));
    ent = _sceneMgr->createEntity("PSpace.mesh");
    nodo->attachObject(ent);
}//Fin createScene

void IntroState::exit(){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void IntroState::pause (){}

void IntroState::resume (){}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}//Fin frameStarted

bool IntroState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin if

void IntroState::keyPressed(const OIS::KeyEvent &e){
    /* Transición al siguiente estado.
     * Espacio --> PlayState */
    switch(e.key){
        case OIS::KC_SPACE:
            changeState(MenuState::getSingletonPtr());
            break;
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin if
}//Fin if

void IntroState::keyReleased(const OIS::KeyEvent &e ){
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }//Fin if
}//Fin keyReleased

void IntroState::mouseMoved(const OIS::MouseEvent &e){}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){}

void IntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){}

IntroState* IntroState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr

IntroState& IntroState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton