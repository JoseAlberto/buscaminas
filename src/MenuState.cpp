#include "MenuState.h"
#include "PlayState.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera); 
    /* Nuevo background colour. */
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
     
    
    if(_render){  
        renderer = &CEGUI::OgreRenderer::bootstrapSystem();    
    }//Fin if

    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");

    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

    if(_render){
        createGUI();
        _render=false;
    }//Fin if
   	
   CEGUI::MouseCursor::getSingleton().show();
    attachButtons();
    formatWin->hide();
    formatWinRecords->hide();
    mostrarBotones();

    _nbombs = 15; /* Por defecto */
    _exitGame = false;
}//Fin enter

void MenuState::exit (){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void MenuState::pause (){
    ocultarBotones();

    CEGUI::MouseCursor::getSingleton().hide();
}//Fin pause

void MenuState::resume (){
    attachButtons();
    mostrarBotones();

    CEGUI::MouseCursor::getSingleton().show();
}//Fin resume

bool MenuState::frameStarted(const Ogre::FrameEvent& evt){
	CEGUI::System::getSingleton().injectTimePulse(_timeSinceLastFrame);
    return true;
}//Fin frameStarted

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
    if(_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void MenuState::keyPressed(const OIS::KeyEvent &e){
    switch(e.key){
        case OIS::KC_ESCAPE:
            _exitGame=true;
            break;
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin if
}//Fin keyPressed

void MenuState::keyReleased(const OIS::KeyEvent &e){}

void MenuState::mouseMoved(const OIS::MouseEvent &e){
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}//Fin mouseMoved

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}//Fin mousePressed

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}//Fin mouseReleased

MenuState* MenuState::getSingletonPtr (){ 
    return msSingleton;
}//Fin getSingletonPtr

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void MenuState::createGUI(){
    /* Sheet */
    sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Sheet");

    /* Config Window */    
    acceptButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","acceptButton");
    checkButton = static_cast<CEGUI::RadioButton*>(
        CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/RadioButton","checkButton"));
    checkButtonM = static_cast<CEGUI::RadioButton*>(
        CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/RadioButton","checkButtonM"));
    checkButtonH = static_cast<CEGUI::RadioButton*>(
        CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/RadioButton","checkButtonH"));

    /* Quit button */
    quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","QuitButton");
    quitButton->setText("Salir");
    quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
    quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::quit, this));

    /* Credits button */
    creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","creditsButton");
    creditsButton->setText("Creditos");

    creditsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.5,0)));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::creditsGUI,this));

    /* Records Button */
    recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","recordsButton");
    recordsButton->setText("Records");

    recordsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));
    recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::recordsGUI, this));

    /* New Game Button */
    gameButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","gameButton");
    gameButton->setText("Nuevo Juego");

    gameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    gameButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.2,0)));
    gameButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::lanzarJuego, this));

    /* Difficulty Button */
    difficultyButton= CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","difficultyButton");
    difficultyButton->setText("Dificultad");

    difficultyButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    difficultyButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.3,0)));
    difficultyButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::difficultyGUI, this));

    checkButton->setText("Facil");
    checkButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    checkButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.3,0)));

    checkButtonM->setText("Normal");
    checkButtonM->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    checkButtonM->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));

    checkButtonH->setText("Dificil");
    checkButtonH->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    checkButtonH->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.5,0)));

    acceptButton->setText("Aceptar");
    acceptButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    acceptButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
    acceptButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::menuGUI,this));

    formatWin = CEGUI::WindowManager::getSingleton().loadWindowLayout("stringFormat.layout");

    exitButton = CEGUI::WindowManager::getSingleton().getWindow("FormatWin/ExitButton");
    exitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    exitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(1-0.25,0),CEGUI::UDim(0.8,0)));
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::menuGUI,this));

    formatWinRecords= CEGUI::WindowManager::getSingleton().loadWindowLayout("RankingFormat.layout");

    exitButtonRecords = CEGUI::WindowManager::getSingleton().getWindow("FormatWinRanking/ExitButton");
    exitButtonRecords->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    exitButtonRecords->setPosition(CEGUI::UVector2(CEGUI::UDim(1-0.25,0),CEGUI::UDim(0.8,0)));
    exitButtonRecords->subscribeEvent(CEGUI::PushButton::EventClicked, 
        CEGUI::Event::Subscriber(&MenuState::menuGUI,this));
}//Fin createGUI

void MenuState::attachButtons(){
    /* Attaching buttons */
    sheet->addChildWindow(checkButton);
    sheet->addChildWindow(checkButtonM);
    sheet->addChildWindow(checkButtonH);

    sheet->addChildWindow(acceptButton);

    sheet->addChildWindow(quitButton);
    sheet->addChildWindow(creditsButton);
    sheet->addChildWindow(recordsButton);
    sheet->addChildWindow(difficultyButton);
    sheet->addChildWindow(gameButton);
    sheet->addChildWindow(formatWin);
    sheet->addChildWindow(formatWinRecords);
    CEGUI::System::getSingleton().setGUISheet(sheet);
}//Fin attachButtons

bool MenuState::quit(const CEGUI::EventArgs &e){
    _exitGame = true;
    return true;
}//Fin quit


bool MenuState::menuGUI(const CEGUI::EventArgs &e){
    /* Sheet */
    CEGUI::WindowManager::getSingleton().getWindow("Sheet");
    formatWin->hide();

    if(checkButton->isSelected()){
        _nbombs = 15;
    }//Fin if

    if(checkButtonM->isSelected()){
        _nbombs = 20;
    }//Fin if

    if(checkButtonH->isSelected()){
        _nbombs = 30;
    }//Fin if

    ocultarBotones();

    mostrarBotones();
    return true;
}//Fin menuGUI

bool MenuState::creditsGUI(const CEGUI::EventArgs &e){
    /* Setting Text! */
    CEGUI::WindowManager::getSingleton().getWindow(
        "FormatWin/Text1")->setText("[colour='FFFF0000'][font='Batang-26] Jose Alberto Granados Perea");
    CEGUI::WindowManager::getSingleton().getWindow(
        "FormatWin/Text2")->setText("[colour='FFFF0000'][font='Batang-26] Julian Palacios Martin-Albo");
    ocultarBotones();
    /* Attaching buttons */
    exitButton->show();
    formatWin->show();
    

    return true;
}//Fin creaditsGUI

bool MenuState::difficultyGUI(const CEGUI::EventArgs &e){
    ocultarBotones();

    checkButton->show();
    checkButtonH->show();
    checkButtonM->show();

    acceptButton->show();

    return true;
}//Fin difficultyGUI

bool MenuState::recordsGUI(const CEGUI::EventArgs &e){
    /* Exit Window */
    ocultarBotones();
    int minimo=1000000, comprobar=0, anterior=0, ranking=1;
    /* Attaching buttons */
    char c[128];
    char actual[128];
   
    int i=0;
    char nombre[128];
    char nombreaux[128];
    char *pch;


    while(ranking <= 5){
        std::ifstream fe("Records.txt");
        i=0;
        while(!fe.eof()) {
            fe.get(c[i++]);
            if(c[i-1]=='\n'){
                c[i-1]='\0';
                pch = strtok (c, ":");
                strcpy(nombreaux, c);
                    

                while( (pch = strtok( NULL, ":" )) != NULL ){
                    std::cout<<pch<<std::endl;
                    strcpy(actual, pch);
                }//Fin while

                sscanf(actual, "%d", &comprobar);
                if(comprobar<minimo && comprobar>anterior){
                    minimo=comprobar;
                    strcpy(nombre, nombreaux);
                }//Fin if             
                i=0;
            }//Fin if             
        }//Fin while
   
        strcat(nombre, " : ");
        
        char colocarRecords[128]="FormatWinRanking/Text";
        char posicion[10];
        char puntuacion[10];
        sprintf(puntuacion,"%d",minimo);
        sprintf(posicion,"%d",ranking);
        strcat(colocarRecords, posicion);
        strcat(nombre, puntuacion);

        CEGUI::WindowManager::getSingleton().getWindow(
        colocarRecords)->setText(nombre);

        ranking++;
        anterior=minimo;
        minimo=1000000;
        comprobar=0;

        fe.close();
    }//Fin while

    exitButton->show();
    formatWinRecords->show();
   
    return true;
}//Fin recordsGUI

bool MenuState::lanzarJuego(const CEGUI::EventArgs &e){
    PlayState::getSingletonPtr()->setBombs(_nbombs);
    pushState(PlayState::getSingletonPtr());

	return true;
}//Fin lanzarJuego

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id){
    CEGUI::MouseButton ceguiId;

    switch(id){
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }//Fin switch
  return ceguiId;
}//Fin convertMouseButton

void MenuState::ocultarBotones(){
    /* Attaching buttons */
    formatWin->hide();
    formatWinRecords->hide();
    quitButton->hide();
    creditsButton->hide();
    recordsButton->hide();
    difficultyButton->hide();
    gameButton->hide();
    checkButton->hide();
    checkButtonH->hide();
    checkButtonM->hide();
    acceptButton->hide();
}//Fin ocultarBotones

void MenuState::mostrarBotones(){
    quitButton->show();
    creditsButton->show();
    recordsButton->show();
    difficultyButton->show();
    gameButton->show();
}//Fin mostrarBotones
