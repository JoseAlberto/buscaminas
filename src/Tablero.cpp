#include "Tablero.h"

template<> Tablero* Ogre::Singleton<Tablero>::msSingleton = 0;

Tablero::Tablero(){}//Fin constructor

Tablero::~Tablero(){
	limpiarTablero();
}//Fin destructor

void Tablero::crearTablero(int dim, int nbombs){
	_dim = dim;
	_nbombs = nbombs;
	_tab = new Celda*[dim];

	_c_click = (dim * dim) - nbombs;

	for(int x = 0; x < dim; x++){
		_tab[x] = new Celda[dim];
		for(int y = 0; y < dim; y++){
			_tab[x][y].setPosX(x); _tab[x][y].setPosY(y);
		}//Fin for
	}//Fin for

	_bombs.reserve(nbombs);
	rellenar();
}//Fin crearTablero

void Tablero::limpiarTablero(){
	/* Limpiamos el vector de bombas */
	_bombs.clear();
	/* Quitamos el reserve que se hizo */
	std::vector<Celda*>().swap(_bombs);

	/* Limpiamos el mapa */
	_relaciones.clear();

	for (int i = 0; (unsigned)i < _dim; i++){
    	delete [] _tab[i];
	}//Fin for
	delete [] _tab;
}//Fin limpiarTablero

void Tablero::reiniciarTablero(){
	limpiarTablero();
	crearTablero(_dim, _nbombs);
}//Fin reiniciarTablero

void Tablero::rellenar(){
	unsigned int idx, idy;
	/* Reinicio semanilla del random */
	srand(time(NULL));

	/* Mientras que _bombs.size() != _dim */
	while(_bombs.size() != _nbombs){
		/* Saco x e y aleatorios */
		idx = (unsigned)rand() % _dim;
		idy = (unsigned)rand() % _dim;
		/* Si x e y dentro de limites y no es bomba*/
		if(idx >= 0 && idx < _dim && idy >= 0 && idy < _dim){
			if(_tab[idx][idy].getEstado() != -1){
			/* Cambio estado de _tab[x][y] y añado _tab[x][y] a _bombs */
				_tab[idx][idy].setEstado(-1);
				_bombs.push_back(&(_tab[idx][idy]));
			}//Fin if
		}//Fin if
	}//Fin while

	/* Para cada bomba en _bombs */
	for(unsigned int b = 0; b < _bombs.size(); b++){
		/* Incremento en 1 adyacentes si dentro de limites y no son bombas */
		for(int x = _bombs[b]->getPosX() - 1; x <= (_bombs[b]->getPosX() + 1); x++){
			for(int y = _bombs[b]->getPosY() - 1; y <= (_bombs[b]->getPosY() + 1); y++){
				if(x >= 0 && (unsigned)x < _dim && y >= 0 && (unsigned)y < _dim){
					if(_tab[x][y].getEstado() != -1){
						_tab[x][y].setEstado(_tab[x][y].getEstado() + 1);
					}//Fin if
				}//Fin if
			}//Fin for
		}//Fin for
	}//Fin for
}//Fin rellenar

void Tablero::addToMap(Ogre::String nombre_nodo, Celda *cel){
	_relaciones[nombre_nodo] = cel;
}//Fin addToMap

bool Tablero::Click(Ogre::String nombre_nodo){
	return onClick(_relaciones[nombre_nodo]);
}//Fin Click

void Tablero::swapMines(){
	for(unsigned int b = 0; b < _bombs.size(); b++){
		swap(_bombs[b]->getNodo());
		_bombs[b]->setVisitada(true);
	}//Fin for
}//Fin swapMineWin

void Tablero::swap(Ogre::SceneNode *nodo){
	/* Para cada nodo de SceneNode, si esta visible se cambia a no visible
	 * y viceversa. Modo cascada = false */
	nodo->flipVisibility(false);
}//Fin swap

void Tablero::descubrirAdyacentes(Celda *cel){
	/* Para cada adyacente de la celda */
	for(int idx = cel->getPosX() - 1; idx <= (cel->getPosX() + 1); idx++){
		for(int idy = cel->getPosY() - 1; idy <= (cel->getPosY() + 1); idy++){
			/* Si esta dentro de los limites */
			if(idx >= 0 && (unsigned)idx < _dim && idy >= 0 && (unsigned)idy < _dim){
				/* Si esta sin cambiar */
				if(!_tab[idx][idy].getVisitada()){
					/* La ponemos visitada */
					_tab[idx][idy].setVisitada(true);
					/* Descontamos la casilla de las totales */
					_c_click--;
					/* Cambiamos su aspecto */
					swap(_tab[idx][idy].getNodo());
					if(_tab[idx][idy].getEstado() == 0){
						/* La relanzamos para descubrir sus adyacentes */
						descubrirAdyacentes(&(_tab[idx][idy]));
					}//Fin if
				}//Fin if
			}//Fin if
		}//Fin for
	}//Fin for
}//Fin descubrirAdyacentes

bool Tablero::onClick(Celda *cel){
	bool finpartida = false;

	if(!cel->getVisitada()){
		/* Ponemos la celda a visitada */
		cel->setVisitada(true);
		/* Si contiene -1: finjuego */
		if(cel->getEstado() == -1){
			/* A perdido la partida */
			finpartida = true;
		}else{
			/* Descontamos la casilla de totales */
			_c_click--;
			/* Cambiamos la apariencia del nodo */
			swap(cel->getNodo());
			/* Si contiene un 0: recursion */
			if(cel->getEstado() == 0){
				descubrirAdyacentes(cel);
			}//Fin if

			if(_c_click == 0){
				/* Ha ganado la partida */
				finpartida = true;
			}//Fin if
		}//Fin if_else
	}//Fin if

	return finpartida;
}//Fin onClick

int Tablero::getCClick(){
	return _c_click;
}//Fin getCClick

Celda* Tablero::getCelda(int x, int y){
	return &(_tab[x][y]);
}//Fin sobrecarga[]

Celda* Tablero::getCelda(Ogre::String nombre_nodo){
	return _relaciones[nombre_nodo];
}//Fin getCelda

Tablero* Tablero::getSingletonPtr(){
    return msSingleton;
}//Fin getSingletonPtr

Tablero& Tablero::getSingleton(){  
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton