#include "Celda.h"

Celda::Celda():
_visitada(false), 
_estado(0), 
_x(-1), 
_y(-1),
_nodo(NULL){}

int Celda::getPosX() const{
	return _x;
}//Fin getPosX

int Celda::getPosY() const{
	return _y;
}//Fin getPosY

int Celda::getEstado() const{
	return _estado;
}//Fin getEstado

bool Celda::getVisitada() const{
	return _visitada;
}//Fin getVisitada

Ogre::SceneNode* Celda::getNodo() const{
	return _nodo;
}//Fin getNodo

void Celda::setPosX(int x){
	_x = x;
}//Fin setPosX

void Celda::setPosY(int y){
	_y = y;
}//Fin setPosY

void Celda::setEstado(int estado){
	_estado = estado;
}//Fin setEstado

void Celda::setVisitada(bool visitada){
	_visitada = visitada;
}//Fin setVisitada

void Celda::setNodo(Ogre::SceneNode *nodo){
	_nodo = nodo;
}//Fin setNodo