#ifndef TABLERO
#define TABLERO

#include <Ogre.h>
#include <OgreSingleton.h>

#include <ctime>
#include <cstdio>
#include <cstdlib>

#include <map>
#include <vector>

#include <Celda.h>

class Tablero: public Ogre::Singleton<Tablero> {
	public:
		Tablero();
		~Tablero();

		/* Acciones sobre el tablero */
		void limpiarTablero();
		void reiniciarTablero();
		void swapMines();
		bool Click(Ogre::String nombre_nodo);
		void crearTablero(int dim, int nbombs);
		void addToMap(Ogre::String nombre_nodo, Celda *cel);

		/* Consultas */
		int getCClick();
		Celda* getCelda(int x, int y);
		Celda* getCelda(Ogre::String nombre_nodo);

		/* Heredados de Ogre::Singleton */
  		static Tablero& getSingleton ();
  		static Tablero* getSingletonPtr ();
	private:
		int _c_click;
		Celda **_tab;
		unsigned int _dim, _nbombs;
		std::vector<Celda*> _bombs;
		std::map<Ogre::String, Celda*> _relaciones;

		/* Metodos de uso propio */
		void rellenar();
		bool onClick(Celda *cel);
		void swap(Ogre::SceneNode *nodo);
		void descubrirAdyacentes(Celda *cel);
};

#endif