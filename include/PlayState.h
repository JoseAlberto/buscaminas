#ifndef PlayState_H
#define PlayState_H

#include <time.h>
#include <Ogre.h>
#include <OIS/OIS.h>

#include "Tablero.h"
#include "GameState.h"

#define CLICK 1 << 0  /* Mascara para objetos de tipo: clickable */
#define NCLICK 1 << 1  /* Mascara para objetos de tipo: no clickable */

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState(){}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        void setBombs(int nbombs);

        /* Heredados de Ogre::Singleton */
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;
        Ogre::RaySceneQuery *_raySceneQuery;
        Ogre::RenderWindow* _win;

        clock_t t_ini, t_pausa;
        bool _exitGame, _endGame;
        int _puntuacion, _nbombs;

    private:
        void createScene(int n_nodos);
        void createOverlay();
};

#endif
