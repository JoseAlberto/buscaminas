#ifndef EndState_H
#define EndState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>


#include "GameState.h"
#include "Tablero.h"

class EndState : public Ogre::Singleton<EndState>, public GameState{
    public:
        EndState() {
             _creado=false;

             
        }

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        bool mostrarMenu(const CEGUI::EventArgs &e);
        bool mostrarVolver(const CEGUI::EventArgs &e);

        bool salir(const CEGUI::EventArgs &e);


        CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

        void createGUI();       
        void mostrarBotones();
        void ocultarBotones();
        void attachButtons();
        void mostrarRecords();
        void ocultarRecords();
        void setPuntuacion(int puntu);

        /* Heredados de Ogre::Singleton. */
        static EndState& getSingleton ();
        static EndState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        CEGUI::OgreRenderer* _renderer; 
        Ogre::Camera* _camera;
        CEGUI::Window* _sheet;
        CEGUI::Window* _quitButton;
        CEGUI::Window* _acceptButton;
        CEGUI::Window* _editBox;
	    CEGUI::Window* _recordText;
        CEGUI::Window* _acceptRecordButton;
        CEGUI::Editbox* _nameText;
        CEGUI::utf32 code_point;
        float _timeSinceLastFrame;
        const char*_nombre;
        bool _exitGame;
        bool _creado;
        int _puntuacion;
        
};

#endif
