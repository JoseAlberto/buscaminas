#ifndef CELDA
#define CELDA

#include <Ogre.h>

class Celda {
	public:
		Celda();
		/* Metodos de consulta */
		int getPosX() const;
		int getPosY() const;
		int getEstado() const;
		bool getVisitada() const;
		Ogre::SceneNode* getNodo() const;
		/* Metodos de modificacion */
		void setPosX(int x);
		void setPosY(int y);
		void setEstado(int estado);
		void setVisitada(bool visitada);
		void setNodo(Ogre::SceneNode *nodo);
	private:
		bool _visitada;
		int _estado, _x, _y;
		Ogre::SceneNode *_nodo;
};

#endif