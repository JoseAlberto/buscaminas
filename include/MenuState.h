#ifndef MenuState_H
#define MenuState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "GameState.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    public:
       MenuState() {
            _render=true; 
        }

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        
        void createGUI();
        bool quit(const CEGUI::EventArgs &e);
        bool menuGUI(const CEGUI::EventArgs &e);
        bool creditsGUI(const CEGUI::EventArgs &e);
        bool difficultyGUI(const CEGUI::EventArgs &e);
        bool recordsGUI(const CEGUI::EventArgs &e);
        bool lanzarJuego(const CEGUI::EventArgs &e);

        void ocultarBotones();
        void mostrarBotones();
        void attachButtons();

        CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

        /* Heredados de Ogre::Singleton. */
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        float _timeSinceLastFrame;
        CEGUI::OgreRenderer* renderer;    
        CEGUI::Window* sheet;
        CEGUI::Window* quitButton;
        CEGUI::Window* creditsButton;
        CEGUI::Window* formatWin;
        CEGUI::Window* formatWinRecords;
        CEGUI::Window* recordsButton;
        CEGUI::Window* gameButton;
        CEGUI::Window* difficultyButton;
        CEGUI::Window* exitButton;
        CEGUI::Window* exitButtonRecords;
        CEGUI::RadioButton* checkButton;
        CEGUI::RadioButton* checkButtonM;
        CEGUI::RadioButton* checkButtonH;
        CEGUI::Window* acceptButton;
        
        bool _exitGame;
        bool _render;
        int _nbombs;
};

#endif